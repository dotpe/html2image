package html2image

import (
	"bytes"
	"fmt"
	"image/jpeg"
	"log"
	"strconv"
	"testing"
	"time"
)

func test3() {
	start := time.Now()
	x := ImageOptions{
		Input:      "src.html",
		Zoom:       2,
		Format:     "pdf",
		SmartWidth: true,
	}
	x.Output = fmt.Sprintf("output-zoom-%.2f-%d.pdf", x.Zoom, start.Unix())
	b, err := GeneratePDF(&x)
	log.Println("HTML to Image - Execution Time(ms):", (time.Now().UnixNano()-start.UnixNano())/1e6)
	if err != nil {
		log.Println(b, err)
		return
	}
}

func test4() {
	start := time.Now()
	x := ImageOptions{
		BinaryPath: "/usr/local/bin/wkhtmltoimage",
		Input:      "https://gobyexample.com/writing-files",
		Output:     "output-" + strconv.Itoa(int(start.Unix())) + ".jpg",
		Quality:    100,
		Zoom:       4,
	}
	b, err := GeneratePDF(&x)
	log.Println("URL to Image - Execution Time(ms):", (time.Now().UnixNano()-start.UnixNano())/1e6)
	if err != nil {
		log.Println(b, err)
		return
	}
}

func Test1(t *testing.T) {
	test3()
}

func test3_1() {
	start := time.Now()
	x := ImageOptions{
		Input:      "src.html",
		Zoom:       2,
		Format:     "pdf",
		SmartWidth: true,
	}
	y := x
	y.Format = "jpg"
	y.Zoom = 4
	x.Output = fmt.Sprintf("output-zoom-%.2f-%d.pdf", x.Zoom, start.Unix())
	b, err := GenerateImage(&y)
	if err != nil {
		log.Println(b, err)
		return
	}
	jconf, err := jpeg.DecodeConfig(bytes.NewReader(b))
	if err != nil {
		log.Println(b, err)
		return
	}
	x.Height = 200 * jconf.Height / jconf.Width
	x.Width = 200
	b, err = GeneratePDF(&x)
	log.Println("HTML to Image - Execution Time(ms):", (time.Now().UnixNano()-start.UnixNano())/1e6)
	if err != nil {
		log.Println(b, err)
		return
	}
}

func Test2(t *testing.T) {
	test3_1()
}
