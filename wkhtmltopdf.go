// Package html2image provides a simple wrapper around wkhtmltoimage (http://wkhtmltopdf.org/) binary.
package html2image

import (
	"bytes"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/dotpe/mindbenders/errors"
)

// GenerateImage creates an image from an input.
// It returns the image ([]byte) and any error encountered.
func GeneratePDF(options *ImageOptions) ([]byte, error) {
	options.setcachepath()
	arr, err := buildPDFParams(options)
	if err != nil {
		return []byte{}, err
	}

	if options.BinaryPath == "" {
		options.BinaryPath = "wkhtmltopdf"
	}
	cmd := exec.Command(options.BinaryPath, arr...)

	if options.HTML != "" {
		cmd.Stdin = strings.NewReader(options.HTML)
	}
	// output, err := cmd.CombinedOutput()
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		return nil, errors.WrapMessage(err, "error occured :"+stderr.String())
	}
	output := out.Bytes()
	if options.Output == "" && len(output) > 0 {
		trimmed := cleanupOutput(output, options.Format)
		return trimmed, nil
	}
	return output, nil
}

// buildParams takes the image options set by the user and turns them into command flags for wkhtmltoimage
// It returns an array of command flags.
func buildPDFParams(options *ImageOptions) ([]string, error) {
	a := []string{}

	if options.Input == "" {
		return []string{}, errors.New("must provide input")
	}

	// For caching web contents.
	if !options.DisableWebCache {
		a = append(a, "--cache-dir", options.CachePath)
	}
	// silence extra wkhtmltoimage output
	// might want to add --javascript-delay too?
	a = append(a, "-q")
	a = append(a, "--disable-plugins")

	if options.Zoom != 0 {
		a = append(a, "--zoom")
		a = append(a, fmt.Sprintf("%f", options.Zoom))
	}

	if options.Height != 0 {
		a = append(a, "--page-height")
		a = append(a, strconv.Itoa(options.Height))
	}

	if options.Width != 0 {
		a = append(a, "--page-width")
		a = append(a, strconv.Itoa(options.Width))
	}

	if options.CropX != 0 {
		a = append(a, "--crop-x")
		a = append(a, strconv.Itoa(options.CropX))
	}

	if options.CropY != 0 {
		a = append(a, "--crop-y")
		a = append(a, strconv.Itoa(options.CropX))
	}

	if options.CropW != 0 {
		a = append(a, "--crop-w")
		a = append(a, strconv.Itoa(options.CropX))
	}

	if options.CropH != 0 {
		a = append(a, "--crop-h")
		a = append(a, strconv.Itoa(options.CropX))
	}

	// url and output come last
	if options.Input != "-" {
		// make sure we dont pass stdin if we aren't expecting it
		options.HTML = ""
	}

	a = append(a, options.Input)

	if options.Output == "" {
		a = append(a, "-")
	} else {
		a = append(a, options.Output)
	}

	return a, nil
}
