package html2image

import (
	"fmt"
	"log"
	"strconv"
	"testing"
	"time"
)

func test1() {
	start := time.Now()
	x := ImageOptions{
		Input:      "src.html",
		Zoom:       2,
		Format:     "jpg",
		SmartWidth: true,
	}
	x.Output = fmt.Sprintf("output-zoom-%.2f-%d.jpg", x.Zoom, start.Unix())
	b, err := GenerateImage(&x)
	log.Println("HTML to Image - Execution Time(ms):", (time.Now().UnixNano()-start.UnixNano())/1e6)
	if err != nil {
		log.Println(b, err)
		return
	}
}

func test2() {
	start := time.Now()
	x := ImageOptions{
		BinaryPath: "/usr/local/bin/wkhtmltoimage",
		Input:      "https://gobyexample.com/writing-files",
		Output:     "output-" + strconv.Itoa(int(start.Unix())) + ".jpg",
		Quality:    100,
		Zoom:       4,
	}
	b, err := GenerateImage(&x)
	log.Println("URL to Image - Execution Time(ms):", (time.Now().UnixNano()-start.UnixNano())/1e6)
	if err != nil {
		log.Println(b, err)
		return
	}
}

func Test(t *testing.T) {
	test1()
	// test2()
}
